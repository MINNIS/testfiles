Please read the full instructions at /README.md

We dont's want hundreds of MBs in a repo so we provide a script to generate the large files.
By default the script generates 6 files in sizes 1 KB, 10 KB, 100 KB, 1 MB, 10 MB and 100 MB.


Windows
-------
Run: /bin/windows.bat


Linux
-----
Run /bin/unix.sh