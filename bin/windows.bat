fsutil FILE CREATENEW "../filesizes/100MB.txt" 104857600
fsutil FILE CREATENEW "../filesizes/10MB.txt" 10485760
fsutil FILE CREATENEW "../filesizes/1MB.txt" 1048576

fsutil FILE CREATENEW "../filesizes/100KB.txt" 102400
fsutil FILE CREATENEW "../filesizes/10KB.txt" 10240
fsutil FILE CREATENEW "../filesizes/1KB.txt" 1024
