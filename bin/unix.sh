dd if=/dev/zero of=../filesizes/100MB.bin count=1024 bs=102400
dd if=/dev/zero of=../filesizes/10MB.bin count=1024 bs=10240
dd if=/dev/zero of=../filesizes/1MB.bin count=1024 bs=1024

dd if=/dev/zero of=../filesizes/100KB.bin count=1024 bs=100
dd if=/dev/zero of=../filesizes/10KB.bin count=1024 bs=10
dd if=/dev/zero of=../filesizes/1KB.bin count=1024 bs=1
